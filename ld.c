/*
 * This file is part of adrld.
 *
 * adrld is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * adrld is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with adrld.  If not, see <http://www.gnu.org/licenses/>.
 */
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <elf.h>
#include <sys/mman.h>
#include <dlfcn.h>
#include <link.h>
#include <pthread.h>

#include "log.h"

/* make sure sizeof(long) is 4, no larger */
static char _long_size_check_[4 - sizeof(long)];

/* load elf,
 * mmap
 * base addr -> reloc
 * got -> data and stubs
 * jmp to entry
 */

typedef struct {
    Elf32_Shdr      *sh;
    char            *name;
    char            *raw;
} elf_sec_t;

typedef struct {
    Elf32_Phdr      *phdr;
    char            *raw;
} elf_seg_t;

typedef struct {
    char            *name;
    uint32_t        size;
    long            addr;
} elf_sym_t;

struct elf;

struct elf {
    char            *path;
    long            entry;

    long            base_addr;
    size_t          prog_size;
    char            *prog;

    int             sec_cnt;
    elf_sec_t       *secs;
    char            *strtab;

    int             seg_cnt;
    elf_seg_t       *segs; 

    elf_sym_t       *symbols;
    Elf32_Sym       *sym;
    int             sym_cnt;

    Elf32_Dyn       *dyn;
    int             dyn_cnt;

    Elf32_Rel       *relocs;
    int             rel_cnt;

    int             so_cnt;
    struct elf      *so;

    /* link to next so */
    struct elf      *next;
};

struct dl_shared_obj;

struct dl_shared_obj {
    struct dl_shared_obj *next;
    char                 *path;
    void                 *handle;
};

/* __va is virtual addr, i.e. the original addr w/o base */
#define PA(__va) (char *)(elf->prog + (__va))

#define LW(__va) (*(uint32_t *)PA(__va))
#define SW(__va, __v) do { \
    (*(uint32_t *)PA(__va)) = (uint32_t)(__v); \
} while (0)

/* defined in ld_trampoline.S */
extern void
_ld_runtime_resolve(void);

/* defined in libc_obj.c */
extern long
find_stub_obj(char *name);

static struct elf * 
__exec_load(char *path);

static struct dl_shared_obj *dl_sos = NULL;

static int
dl_load_cb(struct dl_phdr_info *info, size_t size, void *data)
{
    if (!info->dlpi_name || !info->dlpi_name[0])
        return 0;

    struct dl_shared_obj *so = calloc(1, sizeof(struct dl_shared_obj));

    so->next = dl_sos;
    dl_sos = so;
    so->path = strdup(info->dlpi_name);
    so->handle = dlopen(so->path, RTLD_NOW);
    if (!so->handle)
        PANIC("dlopen\n");
    INFO("shared lib [%s] loaded\n", so->path);

    return 0;
}

static int      __argc;
static char   **__argv;
static char   **__envp;

typedef int (*libc_entry_t)(int argc, char **argv, char **envp);

static int
__libc_init(void *rawargs, void *onexit, libc_entry_t e, void *structors)
{
    INFO("__libc_init main: %p\n", e);
    return e(__argc, __argv, __envp);
}

static long
lookup_libc_symbol(char *name)
{
    long ret;
    struct dl_shared_obj *so = dl_sos;

    while (so) {
        ret = (long) dlsym(so->handle, name);
        if (ret)
            return ret;
        so = so->next;
    }

    INFO("WARNING: %s not found\n", name);

    return 0;
}

/* See ld_trampoline.S */
long
_ld_fixup(struct elf *elf, int idx, long *got)
{
    Elf32_Rel *rel;
    char *name;
    Elf32_Sym *sym = elf->sym;
    long addr;

    rel = elf->relocs + idx;
    name = &elf->strtab[sym[ELF32_R_SYM(rel->r_info)].st_name];
    addr = lookup_libc_symbol(name);
    if (addr)
        *got = addr;

    DEBUG("elf: %s, idx: %d, name: %s, addr :%lx\n",
         elf->path,
         idx,
         name,
         addr);

    return addr;
}

static int
is_system_so(char *name)
{
    if (strcmp(name, "liblog.so") == 0)
        return 1;
    if (strcmp(name, "libstdc++.so") == 0)
        return 1;
    if (strcmp(name, "libm.so") == 0)
        return 1;
    if (strcmp(name, "libc.so") == 0)
        return 1;
    if (strcmp(name, "libdl.so") == 0)
        return 1;

    return 0;
}

/* may have been loaded by other libs */
static int
has_been_loaded(struct elf *elf, char *name)
{
    struct elf *so = elf->so;

    while (so) {
        if (strcmp(name, so->path) == 0)
            return 1;

        if (has_been_loaded(so, name))
            return 1;

        so = so->next;
    }

    return 0;
}

/* find symbols in .so */
static long
find_glob_obj(struct elf *elf, char *name)
{
    struct elf *so = elf->so;
    int i;
    long ret;

    ret = find_stub_obj(name);
    if (ret)
        return ret;

    while (so) {
        int i;
        elf_sym_t *s;

        s = so->symbols;
        for (i = 0; i < so->sym_cnt; i++, s++) {
            if (strcmp(s->name, name) == 0) {
                if (s->size)
                    return s->addr;
                return 0;
            }
        }
        so = so->next;
    }

    return 0;
}

static void
do_relocs(struct elf *elf)
{
    int i, j, cnt;
    Elf32_Sym *sym = elf->sym;

    for (i = 0; i < elf->sec_cnt; i++) {
        elf_sec_t *sec = elf->secs + i;
        Elf32_Shdr *sh = sec->sh;
        Elf32_Rel *rel;

        if (sh->sh_type != SHT_REL)
            continue;

        cnt = sh->sh_size / sizeof(Elf32_Rel);
        rel = (Elf32_Rel *) sec->raw;

        /* each reloc entry */
        for (j = 0; j < cnt; j++, rel++) {
            uint32_t tmp;
            char *name;
            int rtype = ELF32_R_TYPE(rel->r_info);
            int roffs = rel->r_offset;

            switch (rtype) {
            /* Just blindly do the offset */
            case R_ARM_RELATIVE:
                tmp = LW(roffs);
                tmp += elf->base_addr;
                SW(roffs, tmp);
                DEBUG("reloc: %x %x\n", rel->roffs, tmp);
                break;

            /* May or may not do the reloc, depends on the name */
            case R_ARM_GLOB_DAT:
            case R_ARM_JUMP_SLOT:
                name = &elf->strtab[sym[ELF32_R_SYM(rel->r_info)].st_name];
                /* A special case */
                if (strcmp(name, "__libc_init") == 0) {
                    SW(roffs, (uint32_t)&__libc_init);
                    break;
                }
                tmp = find_glob_obj(elf, name);
                if (!tmp && rtype == R_ARM_JUMP_SLOT) {
                    /* keep the original routine, but do a proper reloc.
                     * Android doesn't support lazy binding, but we do.
                     */
                    tmp = LW(roffs);
                    tmp += elf->base_addr;
                }
                DEBUG("reloc: %x %s : %x\n", roffs, name, tmp);
                SW(roffs, tmp);
                break;

            default:
                PANIC("unknown reloc type\n");
                break;
            }
        }
    }

}

static void
setup_pltgot(struct elf *elf)
{
    int i;
    Elf32_Dyn *dyn;
    int cnt;

    dyn = elf->dyn;
    cnt = elf->dyn_cnt;

    for (i = 0; i < cnt; i++, dyn++) {
        if (dyn->d_tag == DT_PLTGOT) {
            long *got = (long *) (elf->prog + dyn->d_un.d_ptr);

            DEBUG("PLTGOT: %x\n", dyn->d_un.d_ptr);

            /* here's the tricky part, we need to fill in the runtime resolver
             * in the GOT[2], which is 0 at this moment.
             * The pointer to elf is stored in GOT[1].
             * The setup conforms to ld_tramponline.S and the PLT0 code.
             * Note: This is not start of .got section. This is DT_PLTGOT.
             * Android doesn't support lazy binding, but we make it happen.
             */
            if (got[2] || got[1]) {
                INFO("%x %x\n", (unsigned) got[1], (unsigned) got[2]);
                PANIC("Invalid GOT\n");
            }
            got[1] = (long) elf;
            got[2] = (long) _ld_runtime_resolve;

            break;
        }
    }
}

static void
load_so(struct elf *elf)
{
    int i;
    Elf32_Dyn *dyn;
    int cnt;

    dyn = elf->dyn;
    cnt = elf->dyn_cnt;

    for (i = 0; i < cnt; i++, dyn++) {
        if (dyn->d_tag == DT_NEEDED) {
            char *name = &elf->strtab[dyn->d_un.d_val];
            struct elf *so;

            if (is_system_so(name))
                continue;

            if (has_been_loaded(elf, name))
                continue;

            /* recursive call */
            so = __exec_load(name);
            if (!so)
                continue;

            so->next = elf->so;
            elf->so = so;
            elf->so_cnt++;
        }
    }
}

static void
resolve_symbols(struct elf *elf)
{
    Elf32_Sym *esym;
    elf_sym_t *sym;
    int i, cnt;

    esym = elf->sym;
    cnt = elf->sym_cnt;

    sym = calloc(cnt, sizeof(elf_sym_t));
    elf->symbols = sym;

    for (i = 1; i <= cnt; i++, sym++, esym++) {
        sym->size = esym->st_size;
        /* base_addr shall be valid */
        sym->addr = elf->base_addr + esym->st_value;
        sym->name = &elf->strtab[esym->st_name];
    }
}

static void
load_segments(struct elf *elf)
{
    Elf32_Phdr *phdr;
    int i;
    uint32_t addr;
    uint32_t mem_size;
    void *mem;

    /* determin how much memory needed.
     * it should be highest va + its size
     */
    addr = 0;

    for (i = 0; i < elf->seg_cnt; i++) {
        phdr = elf->segs[i].phdr;
        if (phdr->p_type == PT_LOAD) {
            if (phdr->p_vaddr > addr) {
                addr = phdr->p_vaddr;
                mem_size = phdr->p_memsz + addr;
            }
        }
    }

    /* align 0x1000 */
    mem_size = (mem_size + 0x1000) & ~0xfff;
    mem = mmap(NULL,
               mem_size,
               PROT_EXEC|PROT_READ|PROT_WRITE,
               MAP_PRIVATE|MAP_ANONYMOUS,
               -1,
               0);

    if (!mem)
        PANIC("mmap");

    elf->prog = mem;
    elf->prog_size = mem_size;
    elf->base_addr = (long) mem;

    /* copyin */
    for (i = 0; i < elf->seg_cnt; i++) {
        phdr = elf->segs[i].phdr;
        if (phdr->p_type == PT_LOAD) {
            if (phdr->p_filesz) {
                memcpy(mem + phdr->p_vaddr,
                       elf->segs[i].raw,
                       phdr->p_filesz);
            } else {
                /* no filesz should be zeroed */
                memset(mem + phdr->p_vaddr,
                       0,
                       phdr->p_memsz);
            }
        }
    }
}

static struct elf *
__exec_load(char *path)
{
    FILE *fp;
    int ret, i;
    struct elf *elf;
    elf_sec_t *esec;
    elf_seg_t *eseg;

    Elf32_Ehdr ehdr;

    Elf32_Phdr *phdrs; /* All program segment headers */
    Elf32_Shdr *shdrs; /* All section hdrs */
    Elf32_Shdr *sec_namestab; /* section name str table */ 
    Elf32_Shdr *psh; /* tmp pointer */
    char *sec_names; /* char array of section name str table */

    INFO("Load %s\n", path);

    if (!(fp = fopen(path, "r"))) {
        perror("fopen");
        return NULL;
    }

    elf = calloc(1, sizeof(*elf));
    elf->path = strdup(path);

    /* read file header */
    fseek(fp, 0, SEEK_SET);
    ret = fread(&ehdr, sizeof(ehdr), 1, fp);
    if (ret != 1)
        goto out;

    /* read all section headers */
    fseek(fp, ehdr.e_shoff, SEEK_SET);
    shdrs = calloc(ehdr.e_shnum, ehdr.e_shentsize);
    ret = fread(shdrs, ehdr.e_shentsize, ehdr.e_shnum, fp);
    if (ret != ehdr.e_shnum)
        goto out;

    /* read program header */
    fseek(fp, ehdr.e_phoff, SEEK_SET);
    phdrs = calloc(ehdr.e_phnum, ehdr.e_phentsize);
    ret = fread(phdrs, ehdr.e_phentsize, ehdr.e_phnum, fp);
    if (ret != ehdr.e_phnum)
        goto out;
    elf->seg_cnt = ehdr.e_phnum;
    eseg = calloc(ehdr.e_phnum, sizeof(*eseg));
    elf->segs = eseg;

    /* read all segments */
    for (i = 0; i < ehdr.e_phnum; i++, eseg++, phdrs++) {
        eseg->phdr = phdrs;
        if (phdrs->p_filesz) {
            eseg->raw = calloc(1, phdrs->p_filesz);
            fseek(fp, phdrs->p_offset, SEEK_SET);
            ret = fread(eseg->raw, phdrs->p_filesz, 1, fp);
            if (ret != 1)
                goto out;
        }
    }

    /* read section name string table */
    sec_namestab = &shdrs[ehdr.e_shstrndx];
    sec_names = calloc(1, sec_namestab->sh_size);
    fseek(fp, sec_namestab->sh_offset, SEEK_SET);
    ret = fread(sec_names, sec_namestab->sh_size, 1, fp);
    if (ret != 1)
        goto out;

    /* read all sections */
    esec = calloc(ehdr.e_shnum - 1, sizeof(elf_sec_t));
    elf->secs = esec;
    elf->sec_cnt = ehdr.e_shnum - 1;

    /* First section header is null */
    psh = shdrs + 1;

    for (i = 1; i < ehdr.e_shnum; i++, psh++, esec++) {
        char *raw = NULL;

        esec->sh = psh;
        esec->name = &sec_names[psh->sh_name];

        if (psh->sh_type != SHT_NOBITS) {
            raw = calloc(1, psh->sh_size);
            fseek(fp, psh->sh_offset, SEEK_SET);
            ret = fread(raw, psh->sh_size, 1, fp);
            if (ret != 1)
                goto out;
            esec->raw = raw;
        }
        if (psh->sh_type == SHT_DYNAMIC) {
            elf->dyn = (Elf32_Dyn *) raw;
            elf->dyn_cnt = psh->sh_size / sizeof(Elf32_Dyn);
        } else if (psh->sh_type == SHT_DYNSYM) {
            elf->sym = (Elf32_Sym *) raw;
            elf->sym_cnt = psh->sh_size / sizeof(Elf32_Sym);
        } else if (strcmp(esec->name, ".dynstr") == 0) {
            elf->strtab = raw;
        } else if (strcmp(esec->name, ".rel.plt") == 0) {
            elf->relocs = (Elf32_Rel *) raw;
            elf->rel_cnt = psh->sh_size / sizeof(Elf32_Rel);
        }
    }

    /* now all segments and sections are loaded */
    /* load segments with mmap, decide base_addr */
    load_segments(elf);

    /* setup lazy binding */
    setup_pltgot(elf);

    /* symbols are resolved after base_addr exists */
    resolve_symbols(elf);

    /* load DT_NEEDED to collect global functions */
    load_so(elf);

    /* after segments are loaded, the base_addr is decided.
     * hence all the relocs are fixed here.
     */
    do_relocs(elf);

    /* last step, find the entry point.
     * the entry point is _start which will call __libc_init.
     * See https://github.com/android/platform_bionic/blob/master/libc/arch-common/bionic/crtbegin.c
     */
    if (ehdr.e_entry)
        elf->entry = elf->base_addr + ehdr.e_entry;

    fclose(fp);
    INFO("Load %s OK. [%x, %x)\n",
         path,
         (unsigned) elf->base_addr,
         (unsigned) elf->base_addr + elf->prog_size);

    return elf;

out:
    if (elf)
        free(elf);
    perror("fread");
    fclose(fp);

    return NULL;
}

typedef int (*elf_entry_t)(void);

int
exec_load(char *path, int argc, char **argv, char **envp)
{
    struct elf *elf;
    elf_entry_t e;

    /* Collect .so that are loaded by system loader */
    dl_iterate_phdr(dl_load_cb, NULL);

    asm volatile ("bl pthread_self");

    elf = __exec_load(path);
    if (!elf)
        PANIC("elf %s load failure\n", path);

    e = (elf_entry_t) elf->entry;
    DEBUG("jmp to entry: %x\n", (unsigned) e);
    argc--;
    argv++;

    __argc = argc;
    __argv = argv;
    __envp = envp;

    return e();
}
