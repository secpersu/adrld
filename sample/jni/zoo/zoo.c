#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <pthread.h>

#include "bar.h"

int something(int i)
{
    return bar(i);
}

static int x;
static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

static pthread_key_t key;
static pthread_once_t key_once = PTHREAD_ONCE_INIT;

static void
make_key(void)
{
    pthread_key_create(&key, free);
}

static void *
my_task(void *arg)
{
    int *ptr;

    printf("enter kid: %lx %d\n", pthread_self(), x);

#if 0
    pthread_mutex_lock(&lock);
    x++;
    pthread_mutex_unlock(&lock);
#endif

    pthread_once(&key_once, make_key);
    if ((ptr = pthread_getspecific(key)) == NULL) {
        ptr = malloc(4);
        pthread_setspecific(key, ptr);
    }
    *ptr = x;

    printf("leave kid: %lx %d %p\n", pthread_self(), x, ptr);

    return arg;
}

int
main(int argc, char **argv)
{
    int i, j, c;
    pthread_t kids[10];

    printf("Hello, world!\n");

    if (argc == 1)
        goto out;
    c = *argv[1];
    if (!isdigit(c))
        goto out;

    i = atoi(argv[1]);
    j = something(i);

    printf("result: %d %c %c\n", j, toupper(c), tolower(c));

    for (i = 0; i < 10; i++)
        pthread_create(&kids[i], NULL, my_task, NULL);
    for (i = 0; i < 10; i++)
        pthread_join(kids[i], NULL);

    return j;

out:
    printf("usage: %s <int>\n", argv[0]);
    return -1;
}
